"""
QTile plugin: Show sunrise/sunset for 55°×15° in localtime.

Requires Python 3.9+
"""

# Copyright © 2023 Klaus Alexander Seistrup <klaus@seistrup.dk>
# Licensed under the GNU Affero General Public License v3+.
#
# Updated: 2023-02-12

import datetime
import zoneinfo

from math import fmod
from time import time

from libqtile.widget import base

import ephem

# Timezones (TZLOC should probably be configurable)
TZUTC = zoneinfo.ZoneInfo('UTC')
TZLOC = zoneinfo.ZoneInfo('Europe/Copenhagen')

# 30 seconds
SEC30 = datetime.timedelta(seconds=30)

# 55°×15° confluence
CONFLUENCE = ephem.city('Copenhagen')
CONFLUENCE.name = 'Confluence'
CONFLUENCE.lat = '55.0000000'
CONFLUENCE.lon = '15.0000000'
CONFLUENCE.elevation = 4

# The Sun
SOL = ephem.Sun()

# Emoji
SUNRISE = '\N{SUNRISE}'
SUNSET = '\N{SUNSET OVER BUILDINGS}'


class MySuntimes(base.InLoopPollText):
    """A simple widget to show the sunset/sunrise."""

    orientations = base.ORIENTATION_HORIZONTAL
    defaults = [
        ('update_interval', 60.0, 'Update interval for the “clock”')
    ]

    def __init__(self, **config):
        """Missing doc string, but keep flake8 happy."""
        base.InLoopPollText.__init__(self, **config)
        self.add_defaults(MySuntimes.defaults)

    def tick(self):
        """Missing doc string, but keep flake8 happy."""
        self.update(self.poll())
        return self.update_interval - fmod(time() + 10 / 9, self.update_interval)

    def _get_local_time(self, etime):
        utime = etime.datetime().replace(tzinfo=TZUTC)
        return utime.astimezone(TZLOC) + SEC30

    def poll(self):
        """Missing doc string, but keep flake8 happy."""
        city = CONFLUENCE
        # 13:00:00Z == noon at the confluence, standard time
        unow = datetime.datetime.now(TZUTC).replace(
            hour=13,
            minute=0,
            second=0
        )
        city.date = unow
        # These will fail if the sun never rises or sets
        sunrise = self._get_local_time(city.previous_rising(SOL))
        sunset = self._get_local_time(city.next_setting(SOL))
        return f'{SUNRISE} {sunrise:%R} {SUNSET} {sunset:%R}'

# eof
