"""
QTile plugin: Show Decimal Internet Time: DIT.

Requires Python 3.6+
"""

# Copyright © 2022 Klaus Alexander Seistrup <klaus@seistrup.dk>
# Licensed under the GNU Affero General Public License v3+.
#
# Updatex: 2023-02-12

from math import fmod
from time import time

from libqtile.widget import base

DAY = 60 * 60 * 24
H12 = DAY / 2


class MyDIT(base.InLoopPollText):
    """A simple text-based clock that shows Decimal Internet Time: DIT."""

    orientations = base.ORIENTATION_HORIZONTAL
    defaults = [
        ('update_interval', 0.43200, 'Update interval for the clock')
    ]

    def __init__(self, **config):
        """Missing doc string, but keep flake8 happy."""
        base.InLoopPollText.__init__(self, **config)
        self.add_defaults(MyDIT.defaults)

    def tick(self):
        """Missing doc string, but keep flake8 happy."""
        self.update(self.poll())
        return self.update_interval - fmod(time(), self.update_interval)

    def poll(self):
        """Missing doc string, but keep flake8 happy."""
        deka = int(round(((time() - H12) % DAY) * 125 / 108))
        (deka, desek) = divmod(deka, 100)
        (deka, desim) = divmod(deka, 100)
        return f'{deka}.{desim:>02}.{desek:>02} DIT'

# eof
