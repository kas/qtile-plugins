"""Lazify imports of all My*() widgets."""

# Copyright © 2023 Klaus Alexander Seistrup <klaus@seistrup.dk>
# Licensed under the GNU Affero General Public License v3+.
#
# Updated: 2023-02-17

from libqtile.utils import lazify_imports
from libqtile.widget.import_error import make_error

MYWIDGETS = {
    'MyBeats': 'mybeats',
    'MyDIT': 'mydit',
    'MyLoadavg': 'myloadavg',
    'MySuntimes': 'mysuntimes',
    'MyUptime': 'myuptime',
}

(__all__, __dir__, __getattr__) = lazify_imports(MYWIDGETS, __package__, fallback=make_error)

# eof
