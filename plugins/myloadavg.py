"""
QTile plugin: Show current load averages.

Requires Python 3.0+
"""

# Copyright © 2015 Klaus Alexander Seistrup <klaus@seistrup.dk>
# Licensed under the GNU Affero General Public License v3+.
#
# Updated: 2023-02-12

from math import fmod
from time import time

from libqtile.widget import base


class MyLoadavg(base.InLoopPollText):
    """Show current load averages."""

    orientations = base.ORIENTATION_HORIZONTAL
    defaults = [
        ('update_interval', 5, 'Update interval for the widget'),
    ]

    def __init__(self, **config):
        """Missing doc string, but keep flake8 happy."""
        base.InLoopPollText.__init__(self, **config)
        self.add_defaults(MyLoadavg.defaults)

    def tick(self):
        """Missing doc string, but keep flake8 happy."""
        self.update(self.poll())
        return self.update_interval - fmod(time() + 10/9, self.update_interval)

    def poll(self):
        """Missing doc string, but keep flake8 happy."""
        with open('/proc/loadavg', 'r', encoding='utf-8') as fp:
            elms = ['Load'] + (fp.read().split() + ['?', '?', '?'])[:3]
            return ' '.join(elms)
        return 'N/A'

# eof
