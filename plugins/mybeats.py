"""
QTile plugin: Show current time in internet beats.

Requires Python 3.6+
"""

# Copyright © 2015 Klaus Alexander Seistrup <klaus@seistrup.dk>
# Licensed under the GNU Affero General Public License v3+.
#
# Updated: 2023-02-12

from math import fmod
from time import time

from libqtile.widget import base


class MyBeats(base.InLoopPollText):
    """Show current time in internet beats."""

    orientations = base.ORIENTATION_HORIZONTAL
    defaults = [
        ('update_interval', 2.16, 'Update interval for the clock')
    ]

    def __init__(self, **config):
        """Missing doc string, but keep flake8 happy."""
        base.InLoopPollText.__init__(self, **config)
        self.add_defaults(MyBeats.defaults)

    def tick(self):
        """Missing doc string, but keep flake8 happy."""
        self.update(self.poll())
        return self.update_interval - fmod(time(), self.update_interval)

    def poll(self):
        """Missing doc string, but keep flake8 happy."""
        bmt = time() + 3600.0 + 86.4 / 2.0
        day = bmt % 86400.0
        beats = int(round(day / 86.4))
        return f'@{beats:03d}'

# eof
