"""
QTile plugin: Show current uptime.

Requires Python 3.6+
"""

# Copyright © 2015 Klaus Alexander Seistrup <klaus@seistrup.dk>
# Licensed under the GNU Affero General Public License v3+.
#
# Updated: 2023-02-12

from math import fmod
from time import time

from libqtile.widget import base


class MyUptime(base.InLoopPollText):
    """Show current uptime."""

    orientations = base.ORIENTATION_HORIZONTAL
    defaults = [
        ('update_interval', 60, 'Update interval for the widget'),
    ]

    def __init__(self, **config):
        """Missing doc string, but keep flake8 happy."""
        base.InLoopPollText.__init__(self, **config)
        self.add_defaults(MyUptime.defaults)

    def tick(self):
        """Missing doc string, but keep flake8 happy."""
        self.update(self.poll())
        return self.update_interval - fmod(time() + 1/7, self.update_interval)

    def poll(self):
        """Missing doc string, but keep flake8 happy."""
        with open('/proc/uptime', 'r', encoding='utf-8') as fp:
            (secs, _) = (fp.read().split() + ['0', '0'])[:2]
            s = int(round(float(secs)+30))
            (d, s) = divmod(s, 86400)
            (h, s) = divmod(s, 3600)
            (m, s) = divmod(s, 60)
            elms = ['Up']
            if d == 1:
                elms.append('1 day,')
            elif d > 1:
                elms.append(f'{d} days,')
            elms.append(f'{h:02d}:{m:02d}')
            return ' '.join(elms)
        return 'N/A'

# eof
