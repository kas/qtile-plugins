# QTile plugins

Plugins for the QTile window manager

## Plugins

* `mybeats`: Show internet time in beats
* `mydit`: Show Decimal Internet Time: DIT
* `myloadavg`: Show current load average
* `mysuntimes`: Show times for sunrise/sunset at the 55°×15° confluence
* `myuptime`: Show current uptime
